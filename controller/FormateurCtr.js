const formateurModel = require('../model/FormateurModel');
var fs =require('fs');
const multer=require ('multer');
const upload = multer({dest:__dirname+'/uploads/images'});
const jwt = require ('jsonwebtoken');
const bcrypt = require ('bcrypt');

module.exports = {


    ajouterFormateur: function (req, res) {
        var file = __dirname + '/uploads/' + req.file.originalname;
        fs.readFile(req.file.path, function (err, data) {
            fs.writeFile(file, data, function (err) {
                {
                    if (err) {
                        res.json({msg :'erreur'})
                    }
                    else {
                        const formateur = new formateurModel({
                            nom: req.body.nom,
                            prenom: req.body.prenom,
                            email: req.body.email,
                            tel: req.body.tel,
                            CIN: req.body.CIN,
                            tarifHoraire: req.body.tarifHoraire,
                            cv: req.body.cv,
                            specialite: req.body.specialite,
                            password: req.body.password,
                            Formation: req.body.Formation,
                            photo: req.file.originalname
                        });
                        formateur.save(function (err) {

                            if (err) {

                                res.json({state: 'no', msg: 'vous avez un erreur'})

                            }
                            else {

                                res.json({state: 'ok', msg: 'formateur crée avec succées '})
                            }


                        })
                    }
                }
            })
        })
    },



    login : function (req, res) {

        formateurModel.findOne({email : req.body.email}, function(err, userInfo){
            if (err) {
                next(err);
            } else {
                if(bcrypt.compareSync(req.body.password, userInfo.password)) {
                    const token = jwt.sign({

                        id : userInfo._id


                    }, req.app.get('secretKey'), { expiresIn: '1h' });

                    res.json({status:"success", message: "user found!!!", data:{user: userInfo, token:token}});

                }
                else{

                    res.json({status:"error", message: "Invalid email/password!!!", data:null});
                }
            }
        });
    }






};