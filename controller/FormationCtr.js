const Formation = require('../model/FormationModel');
var fs =require('fs');
const multer=require ('multer');
const upload = multer({dest:__dirname+'/uploads/images'});
const jwt = require ('jsonwebtoken');
const bcrypt = require ('bcrypt');

module.exports = {


    ajouterFormation: function (req, res) {
        var file = __dirname + '/uploads/' + req.file.originalname;
        fs.readFile(req.file.path, function (err, data) {
            fs.writeFile(file, data, function (err) {
                {
                    if (err) {
                        res.json({msg :'erreur'})
                    }
                    else {
                        const formation = new Formation({
                            titre: req.body.titre,
                            description: req.body.description,
                            ChargeHoraire: req.body.ChargeHoraire,
                            niveau: req.body.niveau,
                            tags: req.body.tags,
                            programme: req.file.originalname
                        });
                        formation.save(function (err) {

                            if (err) {
console.log(err)
                                res.json({state: 'no', msg: 'vous avez un erreur'})

                            }
                            else {

                                res.json({state: 'ok', msg: 'formation crée avec succées '})
                            }


                        })
                    }
                }
            })
        })
    },



    getAll : function (req, res) {

        Formation.find({}, function (err, data) {

            if (err){

                res.json({message : 'vous avez un erreur' + err})
            }
            else {

                res.json(data)

            }

        })

},


    getById : function (req, res) {

        Formation.findOne({_id:req.params.id}).populate('Formateur').exec(function (err, formation) {

            if (err){

                res.json({message : 'vous avez un erreur' + err})
            }
            else {

                res.json(formation)

            }


        })
 }



};