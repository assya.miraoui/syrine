const CandidatCtr = require ('../controller/candidatCtr');
const router = require ('express').Router();
const multer = require('multer');
const upload = multer({dest:__dirname+'/uploads/images'});




router.post('/ajouterCandidat',upload.single('photo'),CandidatCtr.ajouterCandidat);
router.post('/login',CandidatCtr.login);
router.get('/getAll',CandidatCtr.getAll);
router.get('/afficherImage/:photo',CandidatCtr.afficherImage);





module.exports = router ;