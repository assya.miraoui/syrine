const formationCtr = require ('../controller/formationCtr');
const router = require ('express').Router();
const multer = require('multer');
const upload = multer({dest:__dirname+'/uploads/images'});




router.post('/ajouterFormation',upload.single('programme'),formationCtr.ajouterFormation);
router.get('/getAll', formationCtr.getAll);
router.get('/getById/:id', formationCtr.getById);




module.exports = router ;