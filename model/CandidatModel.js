const mongoose  =require('mongoose');
const admin = require('./admin')

const CandidatSchema = admin.discriminator('candidat', new mongoose.Schema(
    {
        nom : {
            type :String,
            required : false,
            trim : true
        },
        prenom : {
            type :String,
            required : false,
            trim : true
        },

        tel : {
            type :String,
            required : false,
            trim : true
        },
        CIN : {
            type :String,
            required : false,
            trim : true
        },
        tarifHoraire : {
            type :String,
            required :false ,
            trim : true
        },
        photo : {
            type :String,
            required : false,
            trim : true
        },
        cv : {
            type :String,
            required : false,
            trim : true
        },
        specialite : {
            type :String,
            required : false,
            trim : true
        },

        Formation : [{
            type : mongoose.Schema.Types.ObjectID,

            ref : 'Formation'

        }]


    }




));


module.exports = CandidatSchema ;
