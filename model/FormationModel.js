const mongoose = require('mongoose');

const Sessions = new mongoose.Schema({


   dateDebut : {

       type :  Date,
       required : true
   },

    dateFin : {

        type :  Date,
        required : true
    }

});



const FormationSchema = mongoose.model('Formation',new mongoose.Schema({


    titre :{
        type : String,
        required :true,
        trim : true
    },
    description :{
        type : String,
        required :true,
        trim : true
    },
    ChargeHoraire :{
        type : String,
        required :true,
        trim : true
    },
    programme :{
        type : String,
        required :true,
        trim : true
    },
    niveau :{
        type : String,
        required :true,
        trim : true
    },
    tags :{
        type : String,
        required :true,
        trim : true
    },


    Sessions : [Sessions],

    Formateur :{

        type : mongoose.Schema.Types.ObjectID,
        ref : 'Formateur'
    }




}));

module.exports = FormationSchema;