const mongoose = require('mongoose');

const bcrypt = require('bcrypt');
const baseOptions = {
    discriminatorKey: 'typeUser',
    collection: 'user'
};

const adminSchema = mongoose.model('admin', new mongoose.Schema({



    email  : {

        type : String


    },


    password : {
        type : String

    }

})
    .pre('save', function () {
        this.password = bcrypt.hashSync(this.password , 10 )

    })
);


module.exports = adminSchema;