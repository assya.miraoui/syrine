const express =  require ('express');
const cors = require('cors');
const bodyParser = require('body-parser');


const formateurRouter = require('./router/formateurRouter');
const CandidatRouter = require('./router/candidatROuter');
const Formation = require('./router/FormationRouter');
const admin = require('./router/adminRouter');
const db = require('./model/db');

const app = express();


app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(cors('*'));
app.set('secretKey', 'test');

app.use('/formateur', formateurRouter);
app.use('/candidat', CandidatRouter);
app.use('/formation', Formation);
app.use('/admin', admin);



app.listen(3000, function () {

    console.log('server started')
})